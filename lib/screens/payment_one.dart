import 'dart:async';
// import 'dart:ffi';
import 'package:flutter/material.dart';
import 'package:my_app/screens/payment_two.dart';
// import 'package:numberpicker/numberpicker.dart';
import 'package:dropdown_button2/dropdown_button2.dart';

class PaymentOne extends StatefulWidget {
  PaymentOne({Key? key, this.paket}) : super(key: key);
  var paket;
  @override
  State<PaymentOne> createState() => _PaymentOneState(this.paket);
}

class _PaymentOneState extends State<PaymentOne> {
  var paket;
  final List<int> items = [
    25000,
    // 45000,
    // 65000,
  ];
  String? selectedValue;

  _PaymentOneState(this.paket);
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 255, 255, 255),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        toolbarHeight: 70,
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text('Pembayaran'),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(70),
          ),
        ),
      ),
      body: SingleChildScrollView(
        physics: const NeverScrollableScrollPhysics(),
        child: Container(
          margin: EdgeInsets.only(top: 150),
          child: Center(
            // Center is a layout widget. It takes a single child and positions it
            // in the middle of the parent.
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Column(
                  children: [
                    SizedBox(
                      child: Text(
                        "Silahkan pilih paket yang kamu mau",
                        style: TextStyle(color: Colors.grey, fontSize: 24),
                      ),
                    ),
                    SizedBox(
                      height: 150,
                    ),
                    Center(
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton2(
                          isExpanded: true,
                          hint: Row(
                            children: const [
                              // Icon(
                              //   Icons.list,
                              //   size: 16,
                              //   color: Colors.white,
                              // ),
                              // SizedBox(
                              //   width: 4,
                              // ),
                              Expanded(
                                child: Text(
                                  'Pilih paket',
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  ),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ],
                          ),
                          items: items
                              .map((item) => DropdownMenuItem<String>(
                                    value: item.toString(),
                                    child: Text(
                                      'Photo Only : ' +
                                          'Rp. ' +
                                          item.toString() +
                                          ',-',
                                      style: const TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white,
                                      ),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ))
                              .toList(),
                          value: selectedValue,
                          onChanged: (value) {
                            setState(() {
                              selectedValue = value as String;
                              paket = value as String;
                            });
                          },
                          // icon: const Icon(
                          //   Icons.arrow_forward_ios_outlined,
                          // ),
                          iconSize: 14,
                          // iconEnabledColor: Colors.yellow,
                          // iconDisabledColor: Colors.grey,
                          buttonHeight: 50,
                          buttonWidth: 300,
                          buttonPadding:
                              const EdgeInsets.only(left: 14, right: 14),
                          buttonDecoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(14),
                            border: Border.all(
                              color: Colors.white,
                            ),
                            color: Colors.green,
                          ),
                          buttonElevation: 2,
                          itemHeight: 40,
                          itemPadding:
                              const EdgeInsets.only(left: 14, right: 14),
                          dropdownMaxHeight: 200,
                          dropdownWidth: 300,
                          dropdownPadding: null,
                          dropdownDecoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(14),
                            color: Colors.grey,
                          ),
                          dropdownElevation: 8,
                          scrollbarRadius: const Radius.circular(40),
                          scrollbarThickness: 6,
                          scrollbarAlwaysShow: true,
                          offset: const Offset(0, 0),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 200,
                    ),
                    SizedBox(
                      width: 300,
                      height: 40,
                      child: ElevatedButton(
                        child: Text("Pay", style: TextStyle(fontSize: 14)),
                        style: ButtonStyle(
                          foregroundColor:
                              MaterialStateProperty.all<Color>(Colors.white),
                          backgroundColor:
                              MaterialStateProperty.all<Color>(Colors.green),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                              side: BorderSide(color: Colors.white),
                            ),
                          ),
                        ),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => PaymentTwo(
                                      paket: paket == null
                                          ? items.toString()
                                          : paket,
                                    )),
                          );
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 150,
                    ),
                    const SizedBox(
                      child: Text(
                        "If this is trouble occurred, please reach out to our instagram @imagein.of.us or whatsapp at 085213000140",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 13,
                          fontStyle: FontStyle.italic,
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 55,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

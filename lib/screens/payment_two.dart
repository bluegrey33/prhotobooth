import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:my_app/main.dart';
import 'package:http/http.dart' as http;
import 'package:open_file/open_file.dart';
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server.dart';
import 'package:window_manager/window_manager.dart';

class PaymentTwo extends StatefulWidget {
  PaymentTwo({Key? key, required this.paket, this.qrisImage}) : super(key: key);
  final paket;
  var qrisImage;
  @override
  // ignore: no_logic_in_create_state
  State<PaymentTwo> createState() => _PaymentTwoState(paket);
}

class _PaymentTwoState extends State<PaymentTwo> {
  // variables params xendit
  var qrisImage;
  var reference_id;

  var status;

  late Timer _timer;
  int _start = 9999999999;

  @override
  void initState() {
    qrCode();
    startTimer();
    // TODO: implement initState
    print('paket : $paket');
    super.initState();
  }

  void startTimer() {
    const oneSec = Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_start == 0) {
          setState(() {
            timer.cancel();
          });
        } else {
          setState(() {
            _start--;
            // print('print every 1 seconds');
          });

          if (_start % 3 == 0) {
            // print('module 10');
            checkOrder();
            // setState(() {
            //   status = 'INACTIVE';
            // });
          }
        }
      },
    );
  }

  // create qr code / qris
  void qrCode() async {
    var headers = {
      'api-version': '2022-07-31',
      'Authorization':
          'Basic eG5kX2RldmVsb3BtZW50XzNFWXNpZ1dmaXRCbnJaUnlTbjlNMEJMeFZHbjNsMzRBWDdZVk1oUVFmSFNXd2pYbmJNek12WlozbVl2a006',
      'Content-Type': 'application/json',
      'Cookie':
          'incap_ses_7248_2182539=KbGqRvbjnzRx0YvYzhGWZApmfmMAAAAArMLBRIlvYnJ1NeAPKkdUpQ==; nlbi_2182539=03M9bZUN4SP2xrHP/0ZNTgAAAAAVQRWjW9uOXe+5WFg3UeTA',
    };

    var data =
        '{\n   "reference_id": "paket-foto-sesi-${(DateTime.now().millisecondsSinceEpoch / 1000).round()}",\n   "type": "DYNAMIC",\n   "currency": "IDR",\n   "amount": ${paket},\n   "callback_url": "https://yourwebsite.com/callback",\n   "expires_at": "2023-10-23T09:56:43.60445Z"\n}\n';

    var url = Uri.parse('https://api.xendit.co/qr_codes');
    var res = await http.post(url, headers: headers, body: data);

    if (res.statusCode != 201) {
      throw Exception('http.post error: statusCode= ${res.statusCode}');
    }
    // print(res.body);
    Map<String, dynamic> response = json.decode(res.body);
    print(response['id']);
    setState(() {
      qrisImage = response['id'];
      reference_id = response['reference_id'];
    });

    sendMail();
  }

  void sendMail() async {
    // Note that using a username and password for gmail only works if
    // you have two-factor authentication enabled and created an App password.
    // Search for "gmail app password 2fa"
    // The alternative is to use oauth.
    String username = 'bluegrey422@gmail.com';
    String password = 'yidicxkfojvjawma';

    final smtpServer = gmail(username, password);
    // Use the SmtpServer class to configure an SMTP server:
    // final smtpServer = SmtpServer('smtp.domain.com');
    // See the named arguments of SmtpServer for further configuration
    // options.

    // Create our message.
    final message = Message()
      ..from = Address(username, 'Your name')
      ..recipients.add('bluegrey422@gmail.com')
      // ..ccRecipients.addAll(['bluegrey422@gmail.com', 'destCc2@example.com'])
      // ..bccRecipients.add(Address('bluegrey422@gmail.com'))
      ..subject =
          'Dart Mailer library :: 😀 :: ${(DateTime.now().millisecondsSinceEpoch / 1000).round()}'
      ..text =
          'This is the plain text.\nThis is line 2 of the text part. \n this is qr id from user payment photo session app : $reference_id'
      ..html =
          "<h3>Notifikasi qr payment (photobooth session user)</h3>\n<p>Hey! Here's some HTML content, <br> <b>this is user reference_id : ( $reference_id )</b> <br> for payment to qris with xendit</p>";

    try {
      final sendReport = await send(message, smtpServer);
      print('Message sent: ' + sendReport.toString());
      // setState(() {
      //   status = 'INACTIVE';
      // });
    } on MailerException catch (e) {
      print('Message not sent.');
      for (var p in e.problems) {
        print('Problem: ${p.code}: ${p.msg}');
      }
    }
    // DONE
  }

  // do check order is success payment / not
  void checkOrder() async {
    // get qr code by qr id
    var headers = {
      'api-version': '2022-07-31',
      'Authorization':
          'Basic eG5kX2RldmVsb3BtZW50XzNFWXNpZ1dmaXRCbnJaUnlTbjlNMEJMeFZHbjNsMzRBWDdZVk1oUVFmSFNXd2pYbmJNek12WlozbVl2a006',
      'Cookie':
          'incap_ses_7248_2182539=6ABGQkiRxHtTpO7azhGWZJTGf2MAAAAAkJ303c8b8lvOb7fcO1dz2Q==; nlbi_2182539=tpaXXQWVoGl+7m9i/0ZNTgAAAACIGqeYOz1vfstIeb+jj9En',
      'Content-Type': 'application/x-www-form-urlencoded',
    };

    var url = Uri.parse('https://api.xendit.co/qr_codes/${qrisImage}');
    var res = await http.get(url, headers: headers);
    if (res.statusCode != 200) {
      throw Exception('http.get error: statusCode= ${res.statusCode}');
    }
    // print(res.body);

    Map<String, dynamic> response = json.decode(res.body);
    // print('status : ' + response['status']);
    setState(() {
      // qrisImage = response['id'];
      status = response['status'];
    });
  }

  // ignore: unused_field
  var _openResult = 'Unknown';

  Future<void> openFile() async {
    var filePath = "C:/Program Files/dslrBooth/dslrBooth.exe";
    // FilePickerResult result = await FilePicker.platform.pickFiles();

    // if (result != null) {
    //   filePath = result.files.single.path;
    // } else {
    //   // User canceled the picker
    // }
    WidgetsFlutterBinding.ensureInitialized();
    await windowManager.ensureInitialized();

    WindowOptions windowOptions = const WindowOptions(
      size: Size(4000, 2000),
      center: true,
      backgroundColor: Colors.transparent,
      skipTaskbar: false,
      titleBarStyle: TitleBarStyle.normal,
    );
    windowManager.waitUntilReadyToShow(windowOptions, () async {
      await windowManager.hide();
      await windowManager.focus();

      Future.delayed(const Duration(seconds: 30), () async {
        await windowManager.show();
        await windowManager.focus();
      });
    });
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => const MyHomePage(
                title: 'Photobooth',
              )),
    ).then((value) => {});
    Future.delayed(const Duration(milliseconds: 1500), () async {
      // Do something
      final _result = await OpenFile.open(filePath);
      print(_result.message);
      // MoveToBackground.moveTaskToBack();
      // windowManager.hide();
      setState(() {
        _openResult = "type=${_result.type}  message=${_result.message}";
      });
    });
  }

  void _hideWindow() {
    windowManager
        .hide(); // will hide the window and the app will be running in the background
  }

  final paket;
  String? selectedValue;

  _PaymentTwoState(this.paket);
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        toolbarHeight: 70,
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Text('Pembayaran'),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(70),
          ),
        ),
      ),
      body: SingleChildScrollView(
        physics: const NeverScrollableScrollPhysics(),
        // ignore: avoid_unnecessary_containers
        child: Container(
          child: Center(
            // Center is a layout widget. It takes a single child and positions it
            // in the middle of the parent.
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Column(
                  children: [
                    const SizedBox(
                      height: 70,
                    ),
                    const SizedBox(
                      child: Text(
                        "Silahkan pilih paket yang kamu mau",
                        style: TextStyle(color: Colors.grey, fontSize: 24),
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Center(
                      child: DropdownButtonHideUnderline(
                        child: Container(
                          // color: Colors.green,
                          margin: const EdgeInsets.all(15.0),
                          padding: const EdgeInsets.all(18.0),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.white),
                            borderRadius: const BorderRadius.all(
                                Radius.circular(
                                    15.0) //                 <--- border radius here
                                ),
                          ),

                          child: Text(
                            'Photo Only : ' + 'Rp. ' + this.paket + ',-',
                            style: const TextStyle(color: Colors.grey),
                          ),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                          children: [
                            const SizedBox(
                              height: 35,
                            ),
                            // ignore: sized_box_for_whitespace
                            Container(
                              width: 400,
                              child: Container(
                                child: DropdownButtonHideUnderline(
                                  child: Container(
                                    // color: Colors.green,
                                    // margin: const EdgeInsets.all(15.0),
                                    // padding: const EdgeInsets.all(18.0),
                                    // decoration: BoxDecoration(
                                    //   border: Border.all(color: Colors.white),
                                    //   borderRadius: BorderRadius.all(Radius.circular(
                                    //           15.0) //                 <--- border radius here
                                    //       ),
                                    // ),

                                    child: const Text(
                                      'Payment Tutorial : ',
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 45,
                            ),
                            // ignore: sized_box_for_whitespace
                            Container(
                              height: 50,
                              width: 400,
                              // color: Colors.amber[600],
                              // ignore: avoid_unnecessary_containers
                              child: Container(
                                  child: const Text(
                                '1. Masuk ke aplikasi dompet digital anda yang telah mendukung QRIS',
                                style: TextStyle(color: Colors.grey),
                              )),
                            ),
                            // ignore: sized_box_for_whitespace
                            Container(
                              height: 50,
                              width: 400,
                              // color: Colors.amber[600],
                              // ignore: avoid_unnecessary_containers
                              child: Container(
                                  child: const Text(
                                '2. Pindai / Scan QR Code yang tersedia',
                                style: TextStyle(color: Colors.grey),
                              )),
                            ),
                            Container(
                              height: 50,
                              width: 400,
                              // color: Colors.amber[600],
                              // ignore: avoid_unnecessary_containers
                              child: Container(
                                  child: const Text(
                                '3. Akan muncul detail transaksi, pastikan data transaksi sudah sesuai',
                                style: TextStyle(color: Colors.grey),
                              )),
                            ),
                            Container(
                              height: 50,
                              width: 400,
                              // color: Colors.amber[600],
                              // ignore: avoid_unnecessary_containers
                              child: Container(
                                  child: const Text(
                                '4. Selesaikan proses pembayaran anda',
                                style: TextStyle(color: Colors.grey),
                              )),
                            ),
                            Container(
                              height: 50,
                              width: 400,
                              // color: Colors.amber[600],
                              // ignore: avoid_unnecessary_containers
                              child: Container(
                                  child: const Text(
                                '5. Transaksi selesai, simpan bukti pembayaran anda',
                                style: TextStyle(color: Colors.grey),
                              )),
                            ),
                            const SizedBox(
                              height: 35,
                            ),
                          ],
                        ),
                        const SizedBox(
                          width: 45,
                        ),
                        Container(
                          child:
                              // _start != 5
                              // ? const Text(
                              //     'Loading...',
                              //     style: TextStyle(
                              //         color: Colors.grey,
                              //         fontSize: 24,
                              //         fontWeight: FontWeight.bold),
                              //   )
                              // :
                              Image.asset('assets/images/qris3.PNG'),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 45,
                    ),
                    // SizedBox(
                    //   child: Text(
                    //     "Please pay before (tanggal/bulan/tahun) jam",
                    //     style: TextStyle(
                    //       color: Colors.white,
                    //       fontSize: 13,
                    //       fontStyle: FontStyle.italic,
                    //     ),
                    //   ),
                    // ),
                    const SizedBox(
                      width: 700,
                      child: Text(
                        "- NMID (QRIS) : ID1022214602040A01",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 13,
                            fontStyle: FontStyle.italic,
                            fontWeight: FontWeight.w200),
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    const SizedBox(
                      width: 700,
                      child: Text(
                        "- Versi Cetak (QRIS) : ID1022214602040A01",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 13,
                            fontStyle: FontStyle.italic,
                            fontWeight: FontWeight.w200),
                      ),
                    ),
                    const SizedBox(
                      height: 85,
                    ),
                    const SizedBox(
                      child: Text(
                        "If this is trouble occurred, please reach out to our instagram @imagein.of.us or whatsapp at 085213000140",
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 13,
                          fontStyle: FontStyle.italic,
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 35,
                    ),
                    SizedBox(
                      width: 150,
                      height: 40,
                      child: status != 'INACTIVE'
                          ? ElevatedButton(
                              child: const Text(
                                "Tunggu pembayaran",
                                style: TextStyle(
                                  fontSize: 14,
                                ),
                                textAlign: TextAlign.center,
                              ),
                              style: ButtonStyle(
                                foregroundColor:
                                    MaterialStateProperty.all<Color>(
                                        Colors.white),
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        Colors.grey),
                                shape: MaterialStateProperty.all<
                                    RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                    side: const BorderSide(color: Colors.white),
                                  ),
                                ),
                              ),
                              onPressed: () {
                                // code...
                              },
                            )
                          : ElevatedButton(
                              child: const Center(
                                child: Text(
                                  "Mulai foto sesi",
                                  style: TextStyle(fontSize: 14),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              style: ButtonStyle(
                                foregroundColor:
                                    MaterialStateProperty.all<Color>(
                                        Colors.white),
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        Colors.green),
                                shape: MaterialStateProperty.all<
                                    RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                    side: const BorderSide(color: Colors.white),
                                  ),
                                ),
                              ),
                              onPressed: () {
                                openFile();
                              },
                            ),
                    ),
                    const SizedBox(
                      height: 35,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
